import pandas as pd
import numpy as np
import re
from sklearn.feature_extraction import DictVectorizer
from sklearn.neighbors import KNeighborsClassifier
from scipy.stats import spearmanr


def one_hot_dataframe(data, cols, replace=False):
    """ Takes a dataframe and a list of columns that need to be encoded.
        Returns a 3-tuple comprising the data, the vectorized data,
        and the fitted vectorizor.
    """
    vec = DictVectorizer()
    mkdict = lambda row: {cols[i]: row[i] for i in range(len(cols))}
    vecData = pd.DataFrame(vec.fit_transform(map(mkdict, data[cols].values)).toarray())
    vecData.columns = vec.get_feature_names()
    vecData.index = data.index

    # in oder to avoid colinearity
    cols_to_remove = set(cols)
    to_keep_cols = []
    for f in vec.get_feature_names():
        f_base_name = f.split("=")[0]
        if f.split("=")[0] in cols_to_remove:
            cols_to_remove.remove(f_base_name)
        else:
            to_keep_cols += [f]

    if replace:
        data = data.drop(cols, axis=1)
        data.join(vecData[to_keep_cols])
        return data
    else:
        return vecData[to_keep_cols]


def replace_comma(row):
    try:
        return re.sub(r"^(?P<name>\d+)?,(?P<name2>\d+)", r'\1.\2', row)
    except TypeError, _:
        return row


def from_num_to_cat(data, col_num, col_cat, borders):
    data[col_num] = data[col_num].astype(float)
    for i in range(len(borders) - 1):
        low = borders[i]
        high = borders[i + 1]
        data.loc[(data[col_num] >= low) & (data[col_num] < high), col_cat] = '>=%i-%i' % (low, high)


label_column = ['DEATH', 'LKADT_P']
id_columns = ['STUDYID', 'RPT']
numerical_columns = ['BMI', 'ALP', 'ALT', 'AST', 'CA', 'CREAT', 'HB', 'LDH', 'NEU', 'PLT', 'PSA', 'TBILI', 'TESTO',
                     'WBC', 'CREACL', 'NA.', 'MG', 'PHOS', 'ALB', 'TPRO', 'RBC', 'LYM']
categorical_dums_columns = ['NON_TARGET', 'TARGET', 'BONE', 'RECTAL', 'LYMPH_NODES', 'KIDNEYS', 'LUNGS', 'LIVER',
                            'PLEURA', 'OTHER', 'PROSTATE', 'ADRENAL', 'BLADDER', 'PERITONEUM', 'COLON', 'HEAD_AND_NECK',
                            'SOFT_TISSUE', 'STOMACH', 'PANCREAS', 'THYROID', 'ABDOMINAL', 'ORCHIDECTOMY',
                            'PROSTATECTOMY', 'TURP', 'LYMPHADENECTOMY', 'SPINAL_CORD_SURGERY', 'BILATERAL_ORCHIDECTOMY',
                            'PRIOR_RADIOTHERAPY', 'ANALGESICS', 'ANTI_ANDROGENS', 'GLUCOCORTICOID', 'GONADOTROPIN',
                            'BISPHOSPHONATE', 'CORTICOSTEROID', 'IMIDAZOLE', 'ACE_INHIBITORS', 'BETA_BLOCKING',
                            'HMG_COA_REDUCT', 'ESTROGENS', 'ANTI_ESTROGENS', 'ARTTHROM', 'CEREBACC', 'CHF', 'DVT',
                            'DIAB', 'GASTREFL', 'GIBLEED', 'MI', 'PUD', 'PULMEMB', 'PATHFRAC', 'SPINCOMP', 'COPD',
                            'MHBLOOD', 'MHCARD', 'MHCONGEN', 'MHEAR', 'MHENDO', 'MHEYE', 'MHGASTRO', 'MHGEN',
                            'MHHEPATO', 'MHIMMUNE', 'MHINFECT', 'MHINJURY', 'MHINVEST', 'MHMETAB', 'MHMUSCLE',
                            'MHNEOPLA', 'MHNERV', 'MHPSYCH', 'MHRENAL', 'MHRESP', 'MHSKIN', 'MHSOCIAL', 'MHSURG',
                            'MHVASC']
categorical_columns = ['LKADT_REF', 'AGEGRP2', 'RACE_C', 'HGTBLCAT', 'WGTBLCAT', 'REGION_C', 'ECOG_C']
drop_columns_useless = ['DISCONT', 'ENDTRS_C', 'ENTRT_PC', 'PER_REF', 'LKADT_PER', 'TRT2_ID', 'TRT3_ID']
drop_columns_available = ['SMOKE', 'GLEAS_DX', 'TSTAG_DX', 'SMOKFREQ', 'SMOKSTAT', 'HEIGHTB', 'WEIGHTBL', 'BUN', 'CCRC',
                          'GLU', 'CREACLCA']
variables = label_column + id_columns + numerical_columns + categorical_dums_columns + categorical_columns


def treat_numericals(df, option_normalize=None):
    df_train_num = df[numerical_columns]
    df_train_num = df_train_num.applymap(replace_comma)
    df_train_num[numerical_columns] = df_train_num[numerical_columns].astype(float)
    if option_normalize is None:
        return df_train_num
    elif option_normalize == 'centered':
        return df_train_num - df_train_num.mean()
    elif option_normalize == 'scaled':
        return (df_train_num - df_train_num.mean()).divide(df_train_num.sd)
    elif option_normalize == 'min-max':
        denominator = df_train_num.max() - df_train_num.min()
        return (df_train_num - df_train_num.min()).divide(denominator)
    return df_train_num


def treat_categoricals(df):
    df = df.copy()
    df[categorical_dums_columns] = df[categorical_dums_columns].replace(np.nan, 'NO')
    # Initially not available in training data
    from_num_to_cat(df, 'HEIGHTBL', 'HGTBLCAT', [140, 160, 180, 200])
    from_num_to_cat(df, 'WEIGHTBL', 'WGTBLCAT', [10 * b for b in range(4, 16)])
    df_train_cat = df[categorical_columns + categorical_dums_columns]
    return df_train_cat


def treat_labels(df):
    df[label_column + id_columns].replace(np.nan, 'unknown')
    df_death = one_hot_dataframe(df, ['DEATH'])
    df_death['DEATH=YES'][df['LKADT_P'].isnull()] = np.nan
    df_labels = df[['RPT', 'LKADT_P', 'STUDYID']].join(df_death)

    return df_labels


def identify_columns_with_mv(df_train):
    N = df_train.shape[0]
    missing_values_pct = dict()
    for var in df_train:
        nb_miss = sum(df_train[var].isnull())
        missing_values_pct[var] = np.divide(float(nb_miss), float(N))
    return missing_values_pct


def find_correlated_variables(df_train, var_with_missing):
    var_to_match_by_missvar = dict()
    for var_miss in var_with_missing:
        to_match = []
        nan_idx = df_train[var_miss].isnull()
        vars_non_missing = [col for col in df_train.columns
                            if sum(df_train[col].loc[nan_idx].isnull()) == 0]

        critic_p_value = 0.05
        critic_rho = 0.1
        while (len(to_match) == 0):
            for var in vars_non_missing:
                rho, pval = spearmanr(df_train[var_miss], df_train[var])
                if (pval <= critic_p_value) and (rho > critic_rho):
                    to_match += [var]
            if len(to_match) == 0:
                critic_p_value *= 1.5
                critic_rho /= 1.5
        var_to_match_by_missvar[var_miss] = to_match
    # print var_to_match_by_missvar
    print [(key, len(value)) for key, value in var_to_match_by_missvar.iteritems()]
    return var_to_match_by_missvar


def replace_mv_by_closest_neighbor(df_train, var_to_match_by_missvar, n_neighbors=3, filled_data_frame=None):
    df_train = df_train.copy()
    if filled_data_frame is None:
        filled_data_frame = df_train

    for missvar, features in var_to_match_by_missvar.iteritems():
        # train is non missing values for targeted column, test in missing values
        nan_idx = df_train[missvar].isnull()
        train_Y = filled_data_frame.loc[~nan_idx, missvar].copy().values
        train_X = filled_data_frame.loc[~nan_idx, features].copy().values
        # remove mv from train columns
        nan_idx_train = np.isnan(train_X).any(axis=1)
        train_X = train_X[1 - nan_idx_train]
        train_Y = train_Y[1 - nan_idx_train]

        test_X = filled_data_frame.loc[nan_idx, features].copy().values

        # print missvar, train_Y.shape, train_X.shape, test_X.shape
        clf = KNeighborsClassifier(n_neighbors=n_neighbors)
        clf.fit(train_X, train_Y)
        preds_Y = clf.predict(test_X)
        df_train[missvar].loc[nan_idx] = preds_Y
    return df_train


def treat_missing_values(df_train, style='knn', n_neighbors=3, df_labels=None):
    missing_values_pct = identify_columns_with_mv(df_train)
    var_to_drop = [var for var, pct in missing_values_pct.iteritems() if pct > 0.5]
    print " Variables to drop because too many missing values:", var_to_drop
    df_train = df_train.drop(var_to_drop, 1)

    var_with_missing = [var for var, pct in missing_values_pct.iteritems() if 0 < pct <= 0.5]
    print " Variables to keep but missing values:", var_with_missing

    if style == 'knn':
        var_to_match_by_missvar = find_correlated_variables(df_train, var_with_missing)
        df_train = replace_mv_by_closest_neighbor(df_train, var_to_match_by_missvar, n_neighbors=n_neighbors)

    if style == 'double_knn':
        var_to_match_by_missvar = find_correlated_variables(df_train, var_with_missing)
        df_train_tmp = replace_mv_by_closest_neighbor(df_train, var_to_match_by_missvar, n_neighbors=n_neighbors)
        var_to_match_by_missvar = find_correlated_variables(df_train_tmp, var_with_missing)
        df_train = replace_mv_by_closest_neighbor(df_train, var_to_match_by_missvar, n_neighbors=n_neighbors,
                                                  filled_data_frame=df_train_tmp)

    elif style == 'median':
        for study in set(df_labels['STUDYID'].values):
            df_train[df_labels == study] = df_train.fillna(df_train[df_labels == study].median())

    elif style == 'mean':
        for study in set(df_labels['STUDYID'].values):
            df_train[df_labels == study] = df_train.fillna(df_train[df_labels == study].mean())

    return df_train


def get_data(path, imputer_style='knn', imputer_neighbors=3, option_normalize=None):
    filename_training = 'prostate_cancer_challenge_data_training/CoreTable_training.csv'
    df_training = pd.read_csv('%s/%s' % (path, filename_training), sep=',')
    filename_testing = 'prostate_cancer_challenge_data_leaderboard/CoreTable_leaderboard.csv'
    df_test = pd.read_csv('%s/%s' % (path, filename_testing), sep=',')
    df_test.index = range(len(df_training), len(df_training) + len(df_test))
    df = pd.concat([df_training, df_test])

    # Replace . by missing values
    df.replace('.', np.nan, inplace=True)

    df_num = treat_numericals(df, option_normalize=option_normalize)
    df_cat = treat_categoricals(df)
    df_labels = treat_labels(df)

    df_train_cat = one_hot_dataframe(df_cat, categorical_columns + categorical_dums_columns)

    df_data = df_num.join(df_train_cat)
    df_data = treat_missing_values(df_data, style=imputer_style, n_neighbors=imputer_neighbors, df_labels=df_labels)

    is_testing = df_labels['DEATH=YES'].isnull()
    df_train_labels = df_labels[~is_testing]
    df_train_data = df_data[~is_testing]

    df_train_cox_output = pd.read_csv('preds_train.csv', sep=';')
    df_train_data = df_train_data.join(df_train_cox_output)

    df_test_labels = df_labels[is_testing]

    df_test_cox_output = pd.read_csv('preds_test.csv', sep=';')
    df_test_cox_output.index = range(len(df_training), len(df_training) + len(df_test))

    df_test_data = df_data[is_testing]
    df_test_data = df_test_data.join(df_test_cox_output)

    return df_train_labels, df_train_data, df_test_labels, df_test_data


def create_csv(path, filename, imputer_style='knn', imputer_neighbors=3, option_normalize=None):
    df_train_labels, df_train_data, df_test_labels, df_test_data = get_data(path, imputer_style=imputer_style,
                                                                            imputer_neighbors=imputer_neighbors,
                                                                            option_normalize=option_normalize)
    df_train_labels.join(df_train_data).to_csv('%s_train.csv' % filename, index=False)
    df_test_labels.join(df_test_data).to_csv('%s_test.csv' % filename, index=False)


path = '/Users/Martin/Dropbox/Genomique/new_data'
# df_labels, df_train = get_data(path, filename)
#create_csv(path, 'final_data', imputer_style='knn', imputer_neighbors=3, option_normalize='centered')
#create_csv(path, 'raw_data', imputer_style='knn', imputer_neighbors=3, option_normalize=None)